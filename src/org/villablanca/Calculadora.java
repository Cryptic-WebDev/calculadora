package org.villablanca;

import java.util.Scanner;

public class Calculadora {
/**
 * Funcion que saca el resto de una division
 * @param dividendo
 * @param divisor
 * @return Resto
 */
	
	public static int pedirNum() {
		Scanner teclado = new Scanner(System.in);
		return teclado.nextInt();
	}
	
	public static int resto(int dividendo, int divisor) {
		int resultado=dividendo%divisor;
		
		return resultado;
	}

	
	public static int suma(int num1, int num2) {
		return num1 + num2;
	}
	
	public static int producto(int num1, int num2) {
		return num1 * num2;
	}
	
	public static int resta(int numero1, int numero2) {
		/**
		 * Función que realiza una resta.
		 * @param int numero1 valor a restar.
		 * @param int numero2 segundo valor a restar.
		 * @return un valor entero resultado de la 
		 * diferencia entre ambos numeros.
		 */
	
		int resultado;
		
		int menor = numero1<numero2?numero1:numero2;
		int mayor = numero1>numero2?numero1:numero2;
		
		return resultado = mayor - menor;
	}


/**
 * Funcion que muestra el menu
 */
	
	public static void mostrarMenu() {
		
		
			System.out.println("1.Sumar");
		
			System.out.println("2.Restar");
			
			System.out.println("3.Multiplicar");
		
			System.out.println("4.Dividir(cociente)");
			
			System.out.println("5.Dividir(resto)");
			
			System.out.println("6.Exponente");
			
			System.out.println("7.Salir");
			
		
		
		}
		
	
	
	

	public static void main(String[] args) {
		int opcion = 0;
		do {
			System.out.print("Introduce un numero: ");
			int num1 = pedirNum();
			
			System.out.print("Introduce otro numero: ");
			int num2 = pedirNum();
			
			System.out.println("Operacion: ");
			mostrarMenu();
			opcion = pedirNum();
			menuPrincipal(opcion, num1, num2);
		} while (opcion != 7);
		System.out.println("Programa finalizado.");
	}
	
	/**
	 * Funcion que calcula la potencia de una base por su exponente
	 * @param base Base entera
	 * @param exponente Exponente entero
	 * @return Resultado de elevar base a exponente
	 */
	public static double potencia(int base, int exponente) {
		return Math.pow(base, exponente);
	}

	public static double cociente(double num1, double num2 ) {
		double resultado;
		resultado=num1/num2;
		return resultado;
	}
	
	public static void menuPrincipal(int opcion, int num1, int num2) {
		int resultado = 0;
		double resultadoDoble = 0.0;
		boolean esDoble = false;
		boolean salir = false;
		switch(opcion) {
		case 1:
			resultado = suma(num1, num2);
			break;
		case 2:
			resultado = resta(num1, num2);
			break;
		case 3:
			resultado = producto(num1, num2);
			break;
		case 4:
			resultadoDoble = cociente(num1, num2);
			esDoble = true;
			break;
		case 5:
			resultado = resto(num1, num2);
			break;
		case 6:
			resultadoDoble = potencia(num1, num2);
			esDoble = true;
			break;
		case 7:
			salir = true;
			break;
		default:
			System.out.println("Opcion incorrecta!!");
			break;
		}
		if (!esDoble && !salir) {
			System.out.println("Resultado: " + resultado);
		} else if (esDoble && salir){
			System.out.println("Resultado: " + resultadoDoble);
		}
		
	}
	

}
